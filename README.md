# **Digital and Soft Skills - Challenge**
## **Controlling a servo motor on raspberry pi using python**
### Noureldeen Nagm 
<br>

-- --
### **Rotatable motor - servo motor SG90**  

A servo motor is a simple device that consists of a DC motor, gears and a feedback based position control system. The main advantage of a servo motor is its ability to hold the angular position of its shaft.

[Buy here](https://www.roboter-bausatz.de/p/sg90-9g-micro-servomotor)  
[datasheet](http://www.ee.ic.ac.uk/pcheung/teaching/DE1_EE/stores/sg90_datasheet.pdf)

**Servo motor specifications**   

- Weight: 9g  
- Dimension: 22.2 x 11.8 x 31mm approx  
- Stall torque: 1.8 kgf·cm  
- Operating speed: 0.1 s/60 degree  
- Operating voltage: 4.8 V (~5V)  
- Dead band width: 10 µs  
- Temperature range: 0 ºC – 55 ºC  
- Rotation range : 180º (90 in each direction)


![01_servo_motor.jpg](./media/01_servo_motor.jpg)  


**Required components** 

- Tower Pro SG90 Servo Motor
- Raspberry Pi
- Connecting wires (male to female wires)
- Power supply
- Input devices: mouse + keyboard  


**Servo motor wire configuration to RaspberryPi**  

The wire configuration steps can be also found here: [instruction link](https://www.electronicshub.org/raspberry-pi-servo-motor-interface-tutorial/) - Electronics Hub  

The SG90 Servo Motor Consists of three Pins: PWM (Orange or Yellow), VCC (Red) and GND (Brown):

1.) The VCC pin must be connected to +5V.  
2.) GND pin must be connected to GND of the power supply.  
3.) PWM or Signal Pin of the Servo Motor must be connected to the PWM Output of the RaspberryPI

![02_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram.jpg](./media/02_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram.jpg) 

![03_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram_2.jpg](./media/03_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram_2.jpg) 

**Data input**  

The servo motor is controlled using PWM control signals.
In PWM technique, you will be sending a pulse of variable width.
The position of the servo motor’s shaft will be set by the width or length of the Pulse.

The frequency of the PWM signal is a fixed value. In our case, SG90 Servo motors have a PWM frequency of 50Hz.  
&rarr; 50Hz i.e. a period of 20ms, the minimum pulse width is 1ms and the maximum pulse width is 2ms. 

![04_raspberry-Pi-Servo-Motor-PWM-Duty-Cycle-Position.jpg](./media/04_raspberry-Pi-Servo-Motor-PWM-Duty-Cycle-Position.jpg)  
<br> 

### **Controlling servo rotation angle - python**  

Using this python code we can control the servo and rotate it to a specific angle degree.

[Tutorial page](https://www.explainingcomputers.com/pi_servos_video.html)

![05_explaining-computers.jpg](./media/05_explaining-computers.jpg)   

**Running the code** 

1.) To run python codes on raspberry pi we use a pre installed program called Thonny python IDE. 

2.) Download the code file [servo.py](./core/servo.py).

3.) Create a new file in thonny and copy paste the code.

4.) Match the raspberry pi GPIO output pin number connected to the PWM pin of the servo with the assigned PWM pin in the code (In this case pin 11).

![06_PWM_pin.jpg](./media/06_PWM_pin.jpg)  

5.) Run the code.

<br>

-- --
contact information: noureldeen.nagm@rwth-aachen.de - Noureldeen Nagm